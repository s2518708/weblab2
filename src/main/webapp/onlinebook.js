function loadBooks(event) {
	if (event.key === "Enter") {
		let input = document.getElementById("search").value;
		var request = new XMLHttpRequest();
		request.onreadystatechange = (response) => {
			if (response.target.readyState == 4 && response.target.status == 200) {
				createTable(response.target.responseText);
			}
		};
		request.open("GET", "http://localhost:8080/onlineStore/rest/" + input, true);
		request.send();
	}
}
function addToCart(bookID) {
	var request = new XMLHttpRequest();
	request.open("POST", "http://localhost:8080/onlineStore/rest/cart", true);
	request.setRequestHeader("Content-Type", "application/json");
	request.setRequestHeader("Accept", "application/json");
	const myObj = [ {
		"item":
		{
			"itemID" : document.getElementById("row" + bookID).cells[0].innerHTML,
			"shortDescription": document.getElementById("row" + bookID).cells[1].innerHTML,
			"longDescription":document.getElementById("row" + bookID).cells[2].innerHTML,
			"cost": document.getElementById("row" + bookID).cells[3].innerHTML
		},
		"numItems": 1
	} ];
	request.send(JSON.stringify(myObj));
	request.onreadystatechange = () => {
		if (request.readyState == 4 && request.status == 204) {
			updateCost();
		}
	}
}

function updateCost() {
		var request = new XMLHttpRequest();
		request.onreadystatechange = (response) => {
			if (response.target.readyState == 4 && response.target.status == 200) {
				document.getElementById("costs").innerText = `Total costs = ${response.target.responseText} Euros`;
			}
		};
		request.open("GET", "http://localhost:8080/onlineStore/rest/cart/costs", true);
		request.send();
}

function createTable(responseText) {
	const items = document.getElementById("items");
	let bookTableHeaders = ["ID", "Short Description", "Long Desciption", "Cost", "Unit"]
	while (items.firstChild) items.removeChild(items.firstChild)

	let bookTable = document.createElement('table');
	let bookTableHead = document.createElement('thead');
	let bookTableHeaderRow = document.createElement('tr');

	bookTableHeaders.forEach(header => {
		let row = document.createElement('th');
		row.innerText = header;
		bookTableHeaderRow.appendChild(row);
	})

	bookTableHead.append(bookTableHeaderRow);
	bookTable.appendChild(bookTableHead);

	let bookTableBody = document.createElement('tbody');
	bookTable.append(bookTableBody);
	items.append(bookTable);
	for (let book of JSON.parse(responseText)) {
		let bookTableBodyRow = document.createElement('tr');
		bookTableBodyRow.id = "row" + book.itemID;

		for (let item of Object.keys(book)) {
			let col = document.createElement('td');
			col.innerHTML = book[item];
			bookTableBodyRow.append(col);
		}
		let unit = document.createElement('td');
		unit.innerHTML = "<button id=\"" + book.itemID + "\" onclick=\"addToCart(this.id)\">Add to cart</button>"
		bookTableBodyRow.append(unit);

		bookTableBody.append(bookTableBodyRow);
	}
}